package com.ravendyne.cardsfx_docexample;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx_docexample.deck.DocsExampleDrawingPane;

import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;

public class DocsExampleUI {
    // ROOT
    BorderPane rootNode;

    // TOOLBAR
    Button btnClearBoard;
    Button btnNewCard;
    Button btnDeleteCard;
    Button btnDeleteConnector;
    
    ChoiceBox<String> cbConnectorChoices;
    ChoiceBox<String> cbCardChoices;
    
    Button btnSetText;
    TextField txtCardText;
    
    // CENTER
    ScrollPane drawingScrollPane;

    // The good stuff
    IDrawingBoard drawing;
    DocsExampleHandler handler;

    public DocsExampleUI() {
        createUI();
        
        wireEventHandling();
    }

    private void wireEventHandling() {
        handler = new DocsExampleHandler(drawing);
        
        btnClearBoard.setOnMouseClicked((e) -> handler.clearBoard());
        btnNewCard.setOnMouseClicked((e) -> handler.newCard());
        btnDeleteCard.setOnMouseClicked((e) -> handler.deleteSelectedCard());
        btnDeleteConnector.setOnMouseClicked((e) -> handler.deleteSelectedConnector());

        cbCardChoices.valueProperty().addListener((observable, oldValue, newValue) -> handler.setNewCardType( newValue.trim() ) );
        cbConnectorChoices.valueProperty().addListener((observable, oldValue, newValue) -> handler.setNewConnectorType( newValue.trim() ) );
        cbCardChoices.getSelectionModel().selectFirst();
        cbConnectorChoices.getSelectionModel().selectFirst();

        btnSetText.setOnMouseClicked((e) -> handler.setSelectedCardText(txtCardText.getText()));
	}

	private void createUI() {
        //---------------------------------------------------------------------
        // TOP
        FlowPane topPane = new FlowPane();
        topPane.setPadding(new Insets(8));
        topPane.setVgap(8);
        topPane.setHgap(8);
        topPane.setId("toolbar");
        
        btnClearBoard = new Button("Clear board");
        btnNewCard = new Button("New card");
        btnDeleteCard = new Button("Delete card");
        btnDeleteConnector = new Button("Delete connector");
        
        cbCardChoices = new ChoiceBox<>();
        cbCardChoices.getItems().addAll("Rectangle", "Circle");
        cbConnectorChoices = new ChoiceBox<>();
        cbConnectorChoices.getItems().addAll("Line", "CubicArrow");
        
        btnSetText = new Button("Set text");
        txtCardText = new TextField();
        txtCardText.setPrefColumnCount(15);
        
        topPane.getChildren().addAll(
                btnClearBoard, 
                btnNewCard,
                btnDeleteCard,
                btnDeleteConnector,
                cbCardChoices,
                cbConnectorChoices,
                btnSetText,
                txtCardText);
        
        //---------------------------------------------------------------------
        // CENTER
        drawingScrollPane = new ScrollPane();
        drawing = new DocsExampleDrawingPane();
        drawingScrollPane.setContent(drawing.getNode());
        
        //---------------------------------------------------------------------
        // ROOT
        rootNode = new BorderPane();
        rootNode.setPrefWidth(1100);
        rootNode.setPrefHeight(700);
        
        rootNode.setTop(topPane);
        rootNode.setCenter(drawingScrollPane);
    }

    public Parent getRootNode() {
        return rootNode;
    }
}
