package com.ravendyne.cardsfx_docexample.deck;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;
import com.ravendyne.cardsfx_docexample.deck.card.DocsExampleCardBuilder;
import com.ravendyne.cardsfx_docexample.deck.card.DocsExampleCardBuilder.DocsCardType;
import com.ravendyne.cardsfx_docexample.deck.connector.DocsExampleConnectorBuilder;
import com.ravendyne.cardsfx_docexample.deck.connector.DocsExampleConnectorBuilder.ConnectorType;
import com.ravendyne.cardsfx_docexample.deck.pin.DocsExamplePinBuilder;

public class DocsExampleCreator implements IDefaultsBuilder {
    private static DocsExampleCreator instance;

    private DocsCardType defaultCardType = DocsCardType.Circle;
    private ConnectorType defaultConnectorType = ConnectorType.Line;
    
    private DocsExampleCreator() {}
    
    public static DocsExampleCreator getInstance() {
        if(instance == null) {
            instance = new DocsExampleCreator();
        }
        
        return instance;
    }

    public DocsExamplePinBuilder pin() {
        return new DocsExamplePinBuilder();
    }

    public DocsExampleCardBuilder card() {
        return new DocsExampleCardBuilder();
    }

    public DocsExampleConnectorBuilder connector() {
        return new DocsExampleConnectorBuilder();
    }

    public void setDefaultCardType(DocsCardType type) {
        defaultCardType = type;
    }

    public void setDefaultconnectorType(ConnectorType type) {
        defaultConnectorType = type;
    }

    @Override
    public ICard newDefaultCard(IDrawingBoard drawing) {
        ICard newCard = DocsExampleCreator.getInstance()
        .card()
        .setType(defaultCardType)
        .setDrawing(drawing)
        .setTitle("a card")
        .build();
        
        switch(defaultCardType) {
        case Circle:
            pin().setCard(newCard).setLocation(Location.Center).build();
            break;
        case Rectangle:
            pin().setCard(newCard).setLocation(Location.Top).build();
            pin().setCard(newCard).setLocation(Location.Bottom).build();
            pin().setCard(newCard).setLocation(Location.Left).build();
            pin().setCard(newCard).setLocation(Location.Right).build();
            break;
        }
        
        return newCard;
    }

    @Override
    public IConnector newDefaultConnector(IDrawingBoard drawing, IPin source, IPin target) {
        IConnector connector = connector()
                .setDrawingBoard(drawing)
                .setSource(source)
                .setTarget(target)
                .setType(defaultConnectorType)
                .build();
        return connector;
    }

}
