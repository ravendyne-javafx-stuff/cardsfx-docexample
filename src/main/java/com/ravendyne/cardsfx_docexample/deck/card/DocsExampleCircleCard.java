package com.ravendyne.cardsfx_docexample.deck.card;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.cardsfx.drawing.AbstractCard;
import com.ravendyne.cardsfx_docexample.deck.pin.DocsExampleTransparentPin;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;

public class DocsExampleCircleCard extends AbstractCard implements ITitledCard {
    
    IPin centerPin;
    Label titleLabel;

    DocsExampleCircleCard(IDrawingBoard drawing, String title) {
        super(drawing);

        titleLabel = new Label( title );
    }

    @Override
    protected Pane createRootUINode() {
        StackPane cardPane = new StackPane();
        
        titleLabel.setTranslateY(-20);

        cardPane.getChildren().add(getContentNode());
        cardPane.getChildren().add(titleLabel);
        
        cardPane.getStyleClass().add("circle");

        return cardPane;
    }

    @Override
    protected Node createContentUINode() {
        Circle uiCircle = new Circle();

        uiCircle.setRadius(DocsExampleTransparentPin.connectorSize + 2);

        return uiCircle;
    }

    @Override
    public String getTitle() {
        return titleLabel.getText();
    }

    @Override
    public void setTitle(String text) {
        titleLabel.setText(text);
    }

    @Override
    public void discard() {
        super.discard();

        // since we added references to these two to our root UI node
        // it helps with memory leak debugging to nullify it here
        titleLabel = null;
        centerPin = null;
    }

    @Override
    protected void removePinFromUI( IPin pin ) {
        getRootNode().getChildren().remove( pin.getRootNode() );
    }

    @Override
    protected void addPinToUI(IPin pin) {
        switch(pin.getLocation()) {
        case Top:
        case Bottom:
        case Left:
        case Right:
            throw new UnsupportedOperationException("Can only add pin to card center.");
        case Center:
            if(centerPin != null) {
                throw new UnsupportedOperationException("Can't add pin to already populated location");
            }

            centerPin = pin;
            StackPane.setAlignment(pin.getRootNode(), Pos.CENTER);

            break;
        }

        getRootNode().getChildren().add( pin.getRootNode() );
    }

}
