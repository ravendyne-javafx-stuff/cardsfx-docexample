package com.ravendyne.cardsfx_docexample.deck.card;

import java.util.Objects;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.ITitledCard;

public class DocsExampleCardBuilder {
    public static enum DocsCardType {
        Circle,
        Rectangle
    }
    
    protected IDrawingBoard pDrawing;
    protected String pTitle;
    
    DocsCardType pCardType;

    public DocsExampleCardBuilder() {
        pCardType = DocsCardType.Rectangle;
    }

    public DocsExampleCardBuilder setDrawing(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        return this;
    }

    public DocsExampleCardBuilder setTitle(String title) {
        Objects.requireNonNull(title);
        pTitle = title;
        return this;
    }

    public DocsExampleCardBuilder setType(DocsCardType type) {
        Objects.requireNonNull(type);
        pCardType = type;
        return this;
    }

    public ITitledCard build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pTitle);

        ITitledCard card = null;
        switch(pCardType) {
        case Circle:
            card = new DocsExampleCircleCard( pDrawing, pTitle );
            break;
        case Rectangle:
            card = new DocsExampleRectangleCard( pDrawing, pTitle );
            break;
        }
        card.buildUI();
        pDrawing.addCard(card);

        return card;
    }

}
