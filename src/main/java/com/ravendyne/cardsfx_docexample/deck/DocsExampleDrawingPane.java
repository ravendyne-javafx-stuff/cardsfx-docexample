package com.ravendyne.cardsfx_docexample.deck;

import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.samples.SampleDrawingPane;

public class DocsExampleDrawingPane extends SampleDrawingPane {
    @Override
    public IDefaultsBuilder getDefaultsBuilder() {
        return DocsExampleCreator.getInstance();
    }
}
