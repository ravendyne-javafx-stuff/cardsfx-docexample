package com.ravendyne.cardsfx_docexample.deck.pin;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;

public class DocsExamplePinBuilder {

    ICard pCard;
    Location pLocation;
    
    public DocsExamplePinBuilder() {
    }
    
    public DocsExamplePinBuilder setCard(ICard card) {
        Objects.requireNonNull(card);
        pCard = card;
        return this;
    }

    public DocsExamplePinBuilder setLocation(Location location) {
        Objects.requireNonNull(location);
        pLocation = location;
        return this;
    }

    public IPin build() {
        IPin pin = new DocsExampleTransparentPin(pCard);
        pin.buildUI();
        pin.setLocation(pLocation);
        pCard.addPin(pin);

        return pin;
    }

}
