package com.ravendyne.cardsfx_docexample.deck.pin;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.drawing.AbstractPin;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

public class DocsExampleTransparentPin extends AbstractPin {
    public static final double connectorSize = 8;

    Pane pinUI;
    private double hotspotXParam;
    private double hotspotYParam;

    DocsExampleTransparentPin(ICard card) {
        super(card);
    }

    @Override
    protected Region createRootUINode() {
        return pinUI;
    }

    @Override
    protected Node createConnectionPoinUINode() {
        pinUI = new Pane();
        return pinUI;
    }

    @Override
    public void setLocation(Location location) {
        super.setLocation(location);
        setDimensions();
        setHotspotData();
    }

    private void setDimensions() {
        switch(getLocation()) {
        case Top:
        case Bottom:
            pinUI.setMaxHeight(connectorSize);
            pinUI.setMaxWidth(-1);
            break;
        case Left:
        case Right:
            pinUI.setMaxHeight(-1);
            pinUI.setMaxWidth(connectorSize);
            break;
        case Center:
            pinUI.setMaxHeight(connectorSize);
            pinUI.setMaxWidth(connectorSize);
            break;
        }
    }
    
    private void setHotspotData() {
        switch(getLocation()) {
        case Top:
            hotspotXParam = 0.5;
            hotspotYParam = 0.0;
            break;
        case Bottom:
            hotspotXParam = 0.5;
            hotspotYParam = 1.0;
            break;
        case Left:
            hotspotXParam = 0.0;
            hotspotYParam = 0.5;
            break;
        case Right:
            hotspotXParam = 1.0;
            hotspotYParam = 0.5;
            break;
        case Center:
            hotspotXParam = 0.5;
            hotspotYParam = 0.5;
            break;
        }
    }

    @Override
    public Point2D getAttachmentPositionLocal() {
        return new Point2D(
                hotspotXParam * getConnectorAttachmentNode().getLayoutBounds().getWidth(),
                hotspotYParam * getConnectorAttachmentNode().getLayoutBounds().getHeight()
           );
    }

}
