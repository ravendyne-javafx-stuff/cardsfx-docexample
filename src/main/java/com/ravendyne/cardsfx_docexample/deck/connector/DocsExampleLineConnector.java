package com.ravendyne.cardsfx_docexample.deck.connector;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.drawing.AbstractConnector;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.shape.Line;

public class DocsExampleLineConnector extends AbstractConnector {
    private Line theLine;
    
    DocsExampleLineConnector(IDrawingBoard drawing, IPin sourcePin, IPin targetPin) {
        super(drawing, sourcePin, targetPin);
    }
    
    @Override
    protected Node createRootUINode() {
        return theLine;
    }

    @Override
    protected Node createConnectorShapeUINode() {
        theLine = new Line();

        return theLine;
    }

    @Override
    public void updatePosition() {
        Point2D sourceConnectionPointOnDrawing = source.getAttachmentPosition();
        Point2D targetConnectionPointOnDrawing = target.getAttachmentPosition();

        final double startX = sourceConnectionPointOnDrawing.getX();
        final double startY = sourceConnectionPointOnDrawing.getY();
        final double endX = targetConnectionPointOnDrawing.getX();
        final double endY = targetConnectionPointOnDrawing.getY();
        
        theLine.setStartX(startX);
        theLine.setStartY(startY);
        theLine.setEndX(endX);
        theLine.setEndY(endY);
    }
}
