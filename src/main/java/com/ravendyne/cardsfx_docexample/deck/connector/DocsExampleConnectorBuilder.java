package com.ravendyne.cardsfx_docexample.deck.connector;

import java.util.Objects;

import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;

public class DocsExampleConnectorBuilder {
    public static enum ConnectorType {
        Line,
        Curve,
    }

    private IDrawingBoard pDrawing;
    private IPin pSource;
    private IPin pTarget;

    private ConnectorType pConnectorType;

    public DocsExampleConnectorBuilder setDrawingBoard(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        pConnectorType = ConnectorType.Curve;
        return this;
    }

    public DocsExampleConnectorBuilder setSource(IPin sourcePin) {
        Objects.requireNonNull(sourcePin);
        pSource = sourcePin;
        return this;
    }

    public DocsExampleConnectorBuilder setTarget(IPin targetPin) {
        Objects.requireNonNull(targetPin);
        pTarget = targetPin;
        return this;
    }

    public DocsExampleConnectorBuilder setType(ConnectorType connectorType) {
        Objects.requireNonNull(connectorType);
        pConnectorType = connectorType;
        return this;
    }

    public IConnector build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pSource);
        Objects.requireNonNull(pTarget);

        IConnector connector = null;
        switch(pConnectorType) {
        case Line:
            connector = new DocsExampleLineConnector(pDrawing, pSource, pTarget);
            break;
        case Curve:
            connector = new DocsExampleBezierConnector(pDrawing, pSource, pTarget);
            break;
        }

        connector.buildUI();
        pDrawing.addConnector(connector);

        return connector;
    }

}
