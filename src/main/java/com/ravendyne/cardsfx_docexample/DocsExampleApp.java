package com.ravendyne.cardsfx_docexample;

import com.ravendyne.cardsfx.css.Styles;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class DocsExampleApp extends Application
{
    public static void main( String[] args )
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //---------------------------------------------------------------------
        // title
        primaryStage.setTitle("CardsFX Example from Documentation");

        //---------------------------------------------------------------------
        // UI
        DocsExampleUI ui = new DocsExampleUI();
        Scene scene = new Scene(ui.getRootNode());

        //---------------------------------------------------------------------
        // styles
        scene.getStylesheets().add(Styles.getDefaultCss());
        scene.getStylesheets().add(DocsExampleApp.class.getResource("app.css").toExternalForm());

        //---------------------------------------------------------------------
        // launch
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
