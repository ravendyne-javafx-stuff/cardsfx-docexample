package com.ravendyne.cardsfx_docexample;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.cardsfx.managers.CardSelectionManager;
import com.ravendyne.cardsfx_docexample.deck.DocsExampleCreator;
import com.ravendyne.cardsfx_docexample.deck.card.DocsExampleCardBuilder.DocsCardType;
import com.ravendyne.cardsfx_docexample.deck.connector.DocsExampleConnectorBuilder.ConnectorType;

import javafx.geometry.Point2D;

public class DocsExampleHandler {
	
	private IDrawingBoard drawing;

    public DocsExampleHandler(IDrawingBoard drawing) {
        this.drawing = drawing;
    }

	public void clearBoard() {
		drawing.clean();
	}

	public void newCard() {
        ICard newCard = DocsExampleCreator.getInstance().newDefaultCard(drawing);

        newCard.setPosition(new Point2D(50, 50));
	}

	public void deleteSelectedCard() {
        ICard card = CardSelectionManager.getInstance(drawing).getSelectedCard();
        if (card != null) {
            drawing.removeCard(card);
            CardSelectionManager.getInstance(drawing).setSelectedCard(null);
        }
	}

	public void deleteSelectedConnector() {
        IConnector connector = CardSelectionManager.getInstance(drawing).getSelectedConnector();
        if(connector != null) {
            drawing.removeConnector(connector);
            CardSelectionManager.getInstance(drawing).setSelectedConnector(null);
        }
	}

	public void setSelectedCardText(String text) {
        ICard card = CardSelectionManager.getInstance(drawing).getSelectedCard();
        if (card != null & card instanceof ITitledCard) {
            ((ITitledCard) card).setTitle( text );
        }
	}

	public void setNewCardType(String type) {
	    switch(type) {
        case "Circle":
            DocsExampleCreator.getInstance().setDefaultCardType(DocsCardType.Circle);
            break;
        case "Rectangle":
            DocsExampleCreator.getInstance().setDefaultCardType(DocsCardType.Rectangle);
            break;
	    }
	}

	public void setNewConnectorType(String type) {
        switch(type) {
        case "Line":
            DocsExampleCreator.getInstance().setDefaultconnectorType(ConnectorType.Line);
            break;
        case "CubicArrow":
            DocsExampleCreator.getInstance().setDefaultconnectorType(ConnectorType.Curve);
            break;
        }
	}

}
