## CardsFX example application

This is working implementation of an application described in CardsFX [technical docs](https://gitlab.com/javafx-stuff/com.ravendyne.cardsfx/blob/master/docs/technical.md) part.


### Dependencies

[CardsFX](https://gitlab.com/ravendyne-javafx-stuff/com.ravendyne.cardsfx)
